This repository contains a set of results obtained for a research investigation. The goal was to say if it is possible to estimate the performance an application would have with different numbers of I/O nodes by 1) estimating its temporal behavior (the I/O phases) from a Darshan aggregated trace and 2) approximating its performance by what was observed with benchmarks that mimic the application's I/O phases. For details (including methodology), see the following research report:

- Francieli Zanon Boito. Estimation of the impact of I/O forwarding on application performance. [Research Report] RR-9366, Inria. 2020, pp.20Available at: https://hal.inria.fr/hal-02969780


